/**
 * This file creates store using reducers
 */

import {createStore, applyMiddleware, compose} from 'redux';
import multi from 'redux-multi';
import thunk from 'redux-thunk';
import reducer from '../reducers/index.js';

// Create store
const store = createStore(
    reducer,
    compose(applyMiddleware(thunk, multi), window.devToolsExtension ? window.devToolsExtension() : f => f
));
// Export store as it is required in other components
export default store;