/**
 * This file has NavBar component
 */
import React, { Component } from 'react';
import logo from '../../assets/images/hotel.svg';
import './navbar.css';
/**
 * Nav bar component definition
 */
class NavBar extends Component {
    /**
     * This method has HTML to be rendered for this component
     */
    render() {
        return (
            <nav className="navbar navbar-expand-sm bg-dark navbar-dark top-nav">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <img src={logo} className="app-logo" alt="Book My Room" />
                        <span className="nav-link text">Book My Room</span>
                    </li>
                </ul>
            </nav>
        );
    };
}
// Exportd as it needs to be used in other components.
export default NavBar;