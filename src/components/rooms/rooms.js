/**
 * This file handles display of rooms list
 */
import React, { Component } from 'react';
import Tile from '../../components/tile/tile.js';
import { connect } from 'react-redux';
import moment from 'moment';
import * as myProductActions from '../../actions/productactions.js';
/**
 * Rooms component definition
 */
class Rooms extends Component {
    constructor(props){
        super(props);        
    }
    /**
     * Retrives product when this component rendered in DOM
     */
    componentDidMount() {    
        this.props.getProducts();
      }
      /**
       * Checkes whether booked start date and end date lies in user selected start date and end date
       * @param {*} startDate : already booked start date
       * @param {*} endDate : already booked end date
       * @param {*} dateToCheckStart : user selected start date
       * @param {*} dateToCheckEnd : user selected end date
       */
      isBookedDateInDateRange(startDate, endDate, dateToCheckStart, dateToCheckEnd){
        let minDate = startDate;
        let maxDate = endDate;
        let checkDateStart = dateToCheckStart;
        let checkDateEnd = dateToCheckEnd;        
        // Check whether already booked start date lies between user selected start date and end date
        let sameOrAfterBookedMinDate = moment(minDate).isSameOrAfter(checkDateStart); 
        let sameOrBeforeBookedMinDate = moment(minDate).isSameOrBefore(checkDateEnd);
        // Check whether already booked end date lies between user selected start date and end date
        let sameOrAfterBookedMaxDate = moment(maxDate).isSameOrAfter(checkDateStart);
        let sameOrBeforeBookedMaxDate = moment(maxDate).isSameOrBefore(checkDateEnd);
        // If range present return true else false
        return (sameOrAfterBookedMinDate && sameOrBeforeBookedMinDate) || (sameOrAfterBookedMaxDate && sameOrBeforeBookedMaxDate);
      }
      /**
       * Check wether dateToCheck lies in startDate and endDate
       * As this function is reusable all the parameters are pased as per the check required
       * @param {*} startDate : start date
       * @param {*} endDate : end date
       * @param {*} dateToCheck : date to check between range
       */
      isInDateRange(startDate, endDate, dateToCheck) {       
          let minDate = startDate;
          let maxDate = endDate;
          let checkDate = dateToCheck;         
          let sameOrAfter = moment(checkDate).isSameOrAfter(minDate);
          let sameOrBefore = moment(checkDate).isSameOrBefore(maxDate);
          return (sameOrAfter && sameOrBefore);           
      }
      /**
       * Decides whether tile for the room will be displayed or not
       * @param {*} item : room object
       */
      canShowTile(item) {          
        let isTileAvailable = false;
        let showRoom = true;
        let selectedDateRangeStart = this.props.selectedDateRangeStart;
        let selectedDateRangeEnd = this.props.selectedDateRangeEnd;
        // Tile for room will be displayed if user has selected date range is lies between rooms availableStartDate and availableEndDate
        if(selectedDateRangeStart && selectedDateRangeEnd && this.isInDateRange(item.availableStartDate, item.availableEndDate, selectedDateRangeStart) && this.isInDateRange(item.availableStartDate, item.availableEndDate, selectedDateRangeEnd)) {
            // If user did not book room for any date then tile will be default visible
            isTileAvailable = !item.bookedDates.length ? true : false;
            // Check user selected date range lies in already booked date range then user can not book this room for this date range hence tile won't be visible
            item.bookedDates.map((bookedDetails, bookedDetailsIndex) => {
                    // If first match found for date then prevent user form from check other booked date ranges                    
                    if(showRoom){
                    var isStartDateInRange = this.isInDateRange(bookedDetails.startDate, bookedDetails.endDate, selectedDateRangeStart);
                    var isEndDateInRange = this.isInDateRange(bookedDetails.startDate, bookedDetails.endDate, selectedDateRangeEnd);
                    var areBothDateInRange = this.isBookedDateInDateRange(bookedDetails.startDate, bookedDetails.endDate, selectedDateRangeStart, selectedDateRangeEnd);
                    // Tile won't be visible if either start date, end date lies in user selected date range or both
                    if(isStartDateInRange || isEndDateInRange || areBothDateInRange){
                        isTileAvailable = false; 
                        showRoom = false;                      
                    }else {
                        isTileAvailable = true; 
                    }
                }
                }
              );

        }        
       return isTileAvailable;        
    }
    /**
     * This method renders html for this component
     */
    render() {
        let productsList = this.props.products ? this.props.products : [];
        return (
            <div>
                {
                    productsList.map((item, index) => {                       
                        if(this.canShowTile(item)){
                            return (
                                <div key={'tile-' + index} className="col-md-4 col-sm-12 d-inline-block">                                                                
                                        <Tile product={item}></Tile>                                                               
                                </div>
                            );
                        }
                    })
                }
            </div>
        );
    };
}
/**
 * Map sate to store
 * @param {*} state : state object
 */
const mapPropsToState = state => {
    return {
      products: state.productsData.allProducts
    };
  }
  /**
   * Match 
   * @param {*} dispatch : dispatch method to dispatch actions to change state
   */
  const mapPropsToDispatch = (dispatch) => {
    return {
        getProducts: () =>
        dispatch(myProductActions.getProducts())
    };
  };
   // exported as it will be required by other components and connect to store
  export default connect(mapPropsToState, mapPropsToDispatch)(Rooms);