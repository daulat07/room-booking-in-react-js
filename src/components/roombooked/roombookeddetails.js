/**
 * This file displays the room details before we confirm booking
 * Dislays details on modal
 * Creates extra data to be displayed on modal based on the date range selected, calculates date wise prices
 */
import React from 'react';
import * as myProductActions from '../../actions/productactions.js';
import { connect } from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import moment from 'moment';
import './roombookeddetails.css';
/**
 * Component definition to display modal
 */
class RoomBookedDetails extends React.Component {
  constructor(props) {
    super(props);
    // Method declaration    
    this.toggleModalVisibility = this.toggleModalVisibility.bind(this);
  } 
/**
 * Toggle modal visibility by setting state in store
 */
  toggleModalVisibility() {    
     return this.props.toggleModalVisibility();   
  }
  /**
   * Set state data to local storage on room booking is confirmed
   */
  saveToDatabase(){
    localStorage.setItem("bookingStorage", JSON.stringify(this.props.allProducts));
  }
  /**
   * Update current booked room data to current via store
   */
  saveRoomData() {
    // Set current date to state via store    
    this.props.updateBookedRoomData(this.selectedRoomNewValues);
    // Store state to local storage
    this.saveToDatabase();
  }
  /**
   * Function handles book now butoon click
   */
  handleBookNow() {
    // Toggle modal visibility               
     this.toggleModalVisibility();
     // Saves state to local storage and update current state
     this.saveRoomData();
 }
 /**
  * Retrives data when component is loaded to DOM
  */
  componentDidMount() {
    // Get's modal visibility state     
    this.props.getModalVisibility();
  }
  /**
   * Calculates percentage for given number
   * @param {*} number : number to calculate
   * @param {*} percentage : percentage to be calculated
   */
  percentage(number, percentage) {
    // Calculates percentage and returns by converting it upto 2 decimal places
    return parseFloat(parseFloat((parseFloat(number)/100)*percentage).toFixed(2));
  }
  /**
   * Creates prices date range wise 
   * Calculates total on full date range
   * Calculates GST taxes, discounts, additional prices
   * @param {*} startDate : user selected start date
   * @param {*} endDate : user selected end date
   */
  createDateRangePrice(startDate, endDate) {
    let totalDays = 0;
    let totalPriceBeforeTax = 0;
    let selectedDateRangePrice = [];
    let bookedDates = [{
      "startDate": startDate,
      "endDate": endDate,
      "totalPrice": 0
    }];
    let dateRangePrice = {};
    let newSelectedRoomDetails = {};
    let  newEndDate = new Date(endDate);
    // Create date range and calculates price for each day by traversing from user selected start date and end date.
    for (let newStartDate = new Date(startDate); newStartDate <= newEndDate; newStartDate.setDate(newStartDate.getDate() + 1)) {     
      dateRangePrice = {};      
      totalDays = totalDays + 1;
      // Set date range date
      dateRangePrice.rangeDate = moment(newStartDate).format('MM/DD/YYYY');
      // If room is selected then calculates the per day pricing
      if(this.props.selectedRoom && Object.keys(this.props.selectedRoom).length){
        dateRangePrice.dayPrice = parseFloat(parseFloat(this.props.selectedRoom.basicPrice).toFixed(2));        
        // If date is wekend date then price will be 20% higher than regular/basic price(before taxes)
        if(newStartDate.getDay() == 6 || newStartDate.getDay() == 0) {
          dateRangePrice.dayPrice = parseFloat(parseFloat(dateRangePrice.dayPrice + this.percentage(dateRangePrice.dayPrice, 20)).toFixed(2));
        }
        // Calculates total price for all the days of user selected date range(before tax)
        totalPriceBeforeTax = parseFloat(parseFloat(parseFloat(parseFloat(totalPriceBeforeTax).toFixed(2)) + dateRangePrice.dayPrice).toFixed(2));      
      }else {
        // Date range pricing will be set to 0 if room is not selected
        dateRangePrice.dayPrice = 0;
      }
      // Add date ranges     
      selectedDateRangePrice.push(dateRangePrice);
    }    
    // Calculates 10% discount on total price of all the day pricing if user has selected date range for more than 10 days(before taxes)
    if(totalDays > 10) {
      bookedDates[0].totalPrice = parseFloat(parseFloat(totalPriceBeforeTax - this.percentage(totalPriceBeforeTax, 10)).toFixed(2));
    }else {
      bookedDates[0].totalPrice = totalPriceBeforeTax;
    }

    // 16% GST on total amount calculated
    bookedDates[0].totalPriceWithGst = parseFloat(parseFloat(bookedDates[0].totalPrice + this.percentage(bookedDates[0].totalPrice, 16)).toFixed(2));
    // Set booked dates for selected room
    if(this.props.selectedRoom && Object.keys(this.props.selectedRoom).length){
      bookedDates = [...bookedDates, ...this.props.selectedRoom.bookedDates];
    }
    // Merge details of selected room with existing data    
    newSelectedRoomDetails = {"bookedDates" : bookedDates, 'selectedDateRangePrice': selectedDateRangePrice};
    return Object.assign({}, this.props.selectedRoom, newSelectedRoomDetails);
  }
  /**
   * Creates model to be provided to components render method
   */
  getModal() {
    // Cerate date range prices and also calculates total prices for selected room
    this.selectedRoomNewValues = this.createDateRangePrice(this.props.selectedDateRangeStart, this.props.selectedDateRangeEnd);      
    return (
        <div>        
        <Modal isOpen={this.props.modal} toggle={this.toggleModalVisibility} className={this.props.className}>
          <ModalHeader toggle={this.toggleModalVisibility}>Room Details</ModalHeader>
          <ModalBody>
            <div className="text-success">* Prices will be 20% higher on weekend</div>            
            <div className="detail-seprator">Room Name: {this.selectedRoomNewValues.roomName}</div>
            <div className="detail-seprator">Basic price: {this.selectedRoomNewValues.basicPrice}</div>            
            <div> Per day pricing as follows:           
               <ul className="room-pricing">{
               this.selectedRoomNewValues.selectedDateRangePrice.map((item, index) => {
                return (
                  <li key={'tile-' + index} className="col-md-8 col-sm-2 d-inline-block detail-seprator">                   
                    <span className="text-warning" >Price for date {item.rangeDate} is :</span> <span>{item.dayPrice}</span>
                  </li>                    
                )})
               }
                </ul>              
            </div>
            <div className="detail-seprator">Total Price: {this.selectedRoomNewValues.bookedDates[0].totalPrice}</div>           
            <div className="detail-seprator">GST: {this.percentage(this.selectedRoomNewValues.bookedDates[0].totalPrice, 16)}</div>
            <div>Total Price with GST: {this.selectedRoomNewValues.bookedDates[0].totalPriceWithGst}</div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" value="Book Now" onClick={this.handleBookNow.bind(this)}>Book Now</Button>{' '}
            <Button color="secondary" onClick={this.toggleModalVisibility}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
  /**
   * Renders modal with the selected room details provided
   */
  render() {    
    let selectedRoomData = this.props.selectedRoom ? this.props.selectedRoom : null;
    return (
      selectedRoomData ? this.getModal() : null
    );
  }
}
/**
 * Map state to store
 * @param {*} state : component state
 */
const mapPropsToState = state => {
    return {
        modal: state.productsData.modal,
        selectedRoom: state.productsData.selectedRoom,
        allProducts: state.productsData.allProducts
    };
  }
  /**
   * Map props to actions
   * @param {*} dispatch : dispatch method to dispatch actions to change state
   */
  const mapPropsToDispatch = (dispatch) => {
    return {
         getModalVisibility: () =>
         dispatch(myProductActions.getModalVisibility()),
         updateBookedRoomData: (selectedRoomNewValues) =>
         dispatch(myProductActions.updateBookedRoomData(selectedRoomNewValues)),
         toggleModalVisibility: () =>
         dispatch(myProductActions.toggleModalVisibility())       
    };
  };
  // exported as it will be required by other components and connect to store 
  export default connect(mapPropsToState, mapPropsToDispatch)(RoomBookedDetails);