/**
 * This file is the container for all the major components required for room booking
 * Handles setting and passing correct state to it's children components 
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import NavBar from '../../components/navbar/navbar.js';
import CustomDateRangePicker from '../../components/daterange/daterange.js';
import Rooms from '../../components/rooms/rooms.js';
import  RoomBookedDetails  from '../../components/roombooked/roombookeddetails.js';
import * as myProductActions from '../../actions/productactions.js';

/**
 * Component definition which includes operation to be performed on room, dates  
 */
class Dashboard extends Component {

  constructor(props) {
    super(props);
    // State for this component
    this.state = {
      startDate: null,
      endDate: null,
      focusedInput: null,
      newStartDate: null,
      newEndDate: null
    };    
    
    // Function declaration of this component
    this.searchMyRooms = this.searchMyRooms.bind(this);
    this.setDateChange = this.setDateChange.bind(this);
    this.setFocusedInputChange = this.setFocusedInputChange.bind(this);
  }
 /**
  * Handles search button click and set's appropriate state to pass on to Rooms and RoomBookedDetails components
  */
  searchMyRooms(isCallFromFocusedInput){
    if(!isCallFromFocusedInput){
     this.setState({ newStartDate: moment(this.state.startDate).format('MM/DD/YYYY'), newEndDate: moment(this.state.endDate).format('MM/DD/YYYY')}); 
    } else{
      this.setState({ newStartDate:null, newEndDate: null});
    }   
  }
  /**
   * Set state for start and end date
   * @param {*} startDate : user selected start date
   * @param {*} endDate : user selected end date
   */
  setDateChange(startDate, endDate){    
    this.setState({ startDate, endDate });
  }
  /**
   * This set's focusedInput state
   * @param {*} focusedInput : focusedInput value when date control focused
   */
  setFocusedInputChange(focusedInput){
    this.setState({focusedInput: focusedInput});    
  }
  /**
   * Fetches initial product data by calling it's action on component rendered to DOM 
   */
  componentDidMount() {    
    this.props.getProductsData();
  }

/**
 * This method contains html to be renderd for this component
 */
  render() {
    return (
      <div className="container">     
       <NavBar/>
       <div className="col-md-12 col-sm-12">
          <CustomDateRangePicker setDateChange = {this.setDateChange} setFocusedInputChange = {this.setFocusedInputChange} startDate = {this.state.startDate} endDate = {this.state.endDate} focusedInput = {this.state.focusedInput} />
          <div className="d-inline-block">
            <input type="button" className="btn btn-primary" value="Search Rooms" onClick={() => {this.searchMyRooms()}}/> 
            {' '}<span className="text-danger">*Select start date and end date to search and book rooms</span>
          </div>   
       </div> 
       <Rooms selectedDateRangeStart = {this.state.newStartDate} selectedDateRangeEnd = {this.state.newEndDate}/>   
       <RoomBookedDetails selectedDateRangeStart = {this.state.newStartDate} selectedDateRangeEnd = {this.state.newEndDate}/>    
      </div>      
    );
  }
}
/**
 * Function handles mapping with the state of this component and store component state
 * @param {*} state : state of this component
 */
const mapPropsToState = state => {
  return {
    productsData: state.productsData
  };
}
/**
 * Function handles action mapping
 * @param {*} dispatch 
 */
const mapPropsToDispatch = (dispatch) => {
  return {
    getProductsData: () =>
      dispatch(myProductActions.getProductsData())
  };
};
// Connect this componet store
export default connect(mapPropsToState, mapPropsToDispatch)(Dashboard);