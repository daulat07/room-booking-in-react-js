/**
 * This file has the html for Tile and it's operations
 */

import React from 'react';
import * as myProductActions from '../../actions/productactions.js';
import { connect } from 'react-redux';
import './tile.css';
/**
 * Tile component definition
 */
class Tile extends React.Component {
    constructor(props){
        super(props); 
    }  
    /**
     * set state for selected room/tile
     * tileData: selected room object
     */
    
    book(tileData) {      
        // Set selected room state by calling action in store
        this.props.setSelectedRoom(tileData);
        // toggle modal visibility by calling action in store
        this.props.toggleModalVisibility();    
    }
    /**
     * Returns tile html
     */
    
    getTile(tileData) {
        const style = {
            backgroundImage: 'URL(' + tileData.roomImageSource + ')'
        },
            basicPrice = 'Price ' + tileData.basicPrice;

        return (
            <div className="card tile">
                <div className="card-header bg-primary text-white">{tileData.roomName}</div>
                <div className="card-body" style={style}></div>
                <div className="card-footer">
                    <span className="text-warning pricing">{basicPrice}</span>  
                    <input type="button" className="btn btn-primary float-right" value="Book" onClick={() => {this.book(tileData)}}/>                   
                </div>
            </div>
        );
    }
/**
 * Renders component html
 */
    render() {
        return (
            this.props.product ? this.getTile(this.props.product) : null
        );
    }
}
/**
 * Map state to store
 * @param {*} state : component state
 */
const mapPropsToState = state => {   
    return {
        selectedRoom: state.productsData.selectedRoom
    }
  }
   /**
   * Map props to actions
   * @param {*} dispatch : dispatch method to dispatch actions to change state
   */
  const mapPropsToDispatch = (dispatch) => {
    return {
        setSelectedRoom: (tileData) =>
        dispatch(myProductActions.setSelectedRoom(tileData.roomId)),       
        toggleModalVisibility: () =>
        dispatch(myProductActions.toggleModalVisibility())
    };
  };
  // exported as it will be required by other components and connect to store 
  export default connect(mapPropsToState, mapPropsToDispatch)(Tile);