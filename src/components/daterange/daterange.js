/**
 * This file handles operations for date range selector
 */
import React, { Component } from 'react';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker } from 'react-dates';
import './daterange.css';

/**
 * Component/Class handles airbnb date range picker
 */
class CustomDateRangePicker extends Component {
    constructor(props) {
        super(props);
        // Methods declaration
        this.handleDateChange = this.handleDateChange.bind(this);
        this.setFocusedInputChangeValues = this.setFocusedInputChangeValues.bind(this);        
      }
      /**
       * Set's focused input state to it's parent
       */

    setFocusedInputChangeValues(focusedInput){
        this.props.setFocusedInputChange(focusedInput);
    }
    /**
     * Set's start date and end date state to it's parent
     * @param {*} startDate : user selected start date
     * @param {*} endDate : user selected end date
     */
    handleDateChange(startDate, endDate){
        this.props.setDateChange(startDate, endDate);
    }
    /**
     * This method contains HTMl to be rendered for this component
     */
    render() {
        return (
            <div className="d-inline-block">
                <DateRangePicker
                startDateId="startDate"
                endDateId="endDate"
                startDate={this.props.startDate}
                endDate={this.props.endDate}
                onDatesChange={({ startDate, endDate }) => {this.handleDateChange(startDate, endDate)}}
                focusedInput={this.props.focusedInput}
                onFocusChange={(focusedInput) => { this.setFocusedInputChangeValues(focusedInput)}}
                showClearDates= {true}
                showDefaultInputIcon = {true}
                reopenPickerOnClearDates= {true}                
                />
            </div>
        );
    };
}
// Exported as it is required to be used in other components
export default CustomDateRangePicker;