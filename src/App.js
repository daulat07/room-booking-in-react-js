/**
 * App component is the first component of application
 * Routes are defined here
 */

import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Dashboard from './components/dashboard/dashboard.js';
import { BrowserRouter, Route} from 'react-router-dom';

/**
 * Definition for app component
 */
class App extends Component { 
  // Renders app component
  render() {
    return (
      <BrowserRouter>
         <div>
            <Route  path="/" component={Dashboard} />            
         </div>
      </BrowserRouter>
    );
  }
}
// Export as it will be required by other components
export default App;