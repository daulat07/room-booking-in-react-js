/**
 * This file contain actions to be performed while doing room booking from start to end
 */
import axios from 'axios';
import { constants } from '../action-types/actiontypes.js';

/**
 * Fetches initial product/rooms data form the stub Json file productfile.json
 * Once received data sends to reducer to update state accordingly
 */
export function getProductsData() {
    let apiUrl = '/api-stub/productfile.json';
    return (dispatch) => {
        axios.get(apiUrl)
            .then(response => {
                dispatch({
                    type: constants.GET_INITIAL_PRODUCTS,
                    payload: response.data
                });
            }, error => {
                console.log('Error : ', error);
            });
    };
};
/**
 * Return current available products
 */
export function getProducts() {
    return {
        type: constants.GET_PRODUCTS
    }
}
/**
 * Set's modal initial state
 */
export function getModalVisibility() {
    return {        
        type: constants.GET_MODAL_VISIBILITY
    }
}
/**
 * Handles modal toggling functionality
 */
export function toggleModalVisibility() {
    return {        
        type: constants.TOGGLE_MODAL_VISIBILITY
    }
}
/**
 * Set's current selected data to state by calling it's reducer action
 * @param {*} tileId: roomid i.e tileid
 */
export function setSelectedRoom(tileId) {
    return {
        payload: tileId,
        type: constants.SET_SELECTED_ROOM
    }
}
/**
 * Get current selected room/tile state
 */
export function getSelectedRoom() {
    return {        
        type: constants.GET_SELECTED_ROOM
    }
}
/**
 * Updates the details of current modified room to state using reducer
 * @param {*} updateBookedRoomData : current modified room object
 */
export function updateBookedRoomData(updateBookedRoomData) {
    return {
        payload: updateBookedRoomData,        
        type: constants.UPDATE_BOOKED_ROOM_DATA
    }
}


