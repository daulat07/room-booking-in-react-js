/**
 * This file combines reducers
 */

import { combineReducers }  from 'redux';
import * as myProductReducers from './productreducers.js';
// Export combined reducer by mapping state 
export default combineReducers (
    {productsData: myProductReducers.myProductReducer});
