
/**
 * This file has state and actions to receive current state and return new state
 */

import { constants } from '../action-types/actiontypes.js';
// State storage
const initialState = {
  allProducts: [],
  selectedRoom: {},
  modal: false
};
/**
 * Reducer to perform different actions on state object
 * @param {*} state : state
 * @param {*} action : action
 */
export  function myProductReducer(state = initialState, action) {
    switch (action.type) {
      case constants.GET_INITIAL_PRODUCTS:
      // This action processes response from stub json file i.e initial response and checks if any rooms are booked in local storage
      // If any room is booked(found in local storage) then update those booked date ranges in initial response so that room/tile visibility will be
      // calculated in further actions. 
      // returns new state after update
      let newProductState = action.payload;
      let newResponseState = [];
      let localStorageProducts = localStorage.getItem('bookingStorage');
      let localStorageItems = [];
                        // Process only if local storage has data for already booked rooms
                        if(localStorageProducts){
                          localStorageItems = JSON.parse(localStorageProducts);
                          newResponseState = newProductState.map(
                            (product, productIndex) => {
                              localStorageItems.map(
                                (storageProduct, storageProductIndex) => {
                                  // If any room from initial response matches to the storage room then update bookedDates in the initial response for that room
                                  if (product.roomId === storageProduct.roomId) {
                                      product.bookedDates = storageProduct.bookedDates;
                                  }            
                                }
                          );
                          return product;
                        }
                      );                           
          }
          // Return new state
        return {
          ...state,
          allProducts: newResponseState.length ? newResponseState : newProductState
        }
        case constants.GET_PRODUCTS:
        // Return state
        return {          
         allProducts :  state.allProducts
        }

        case constants.SET_SELECTED_ROOM:
        // This action set state for selected room 
        let data = {};
        const newState = Object.assign({}, state);
         newState.allProducts.map(
          (product, productIndex) => {
            if (product.roomId === action.payload) {
              data = product;
            }            
          }
        );
        // Return new state        
        return Object.assign({}, state, {selectedRoom: data});

        case constants.GET_SELECTED_ROOM:
        // Return state of selected room
        return {
          selectedRoom: state.selectedRoom
        }

        case constants.GET_MODAL_VISIBILITY:
        // Return state of model visibility
        return {
          ...state
        }

        case constants.TOGGLE_MODAL_VISIBILITY:
         // toggles state of model visibility and returns new state
        return {
          ...state,
          modal: !state.modal
        } 
        
        case constants.UPDATE_BOOKED_ROOM_DATA:
        // Update state for booked room      
      const newStateData = Object.assign({}, state);      
      const newValuesData = newStateData.allProducts.map(
      (product, productIndex) => {
        if (product.roomId === action.payload.roomId) {            
            product.bookedDates = action.payload.bookedDates;
        }
        return product;          
      }
    ); 

    // Return new state
    return Object.assign({}, state, { allProducts: newValuesData });
   
      default:
      // Default state return if any action doesnot match
        return state;
    }
  }
